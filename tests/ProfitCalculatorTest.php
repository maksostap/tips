<?php

use Tips\src\ProfitCalculator;
use PHPUnit\Framework\TestCase;

class ProfitCalculatorTest extends TestCase
{
    const LOST = 0;

    const WON = 1;

    const HALF_LOST = 2;

    const HALF_WON = 3;

    const VOIDED = 4;

    static public $picks = array(
        0 => array(
            'stake' => '1',
            'odd' => '3.20',
            'status' => self::LOST,
            'result' =>-1
        ),

        1 => array(
            'stake' => '5',
            'odd' => '2.00',
            'status' => self::WON,
            'result'=>5
        ),

        2 => array(
            'stake' => '1',
            'odd' => '8.30',
            'status' => self::HALF_WON,
            'result'=>3.15
        ),

        3 => array(
            'stake' => '2',
            'odd' => '5.56',
            'status' => self::HALF_LOST,
            'result' => -1
        ),

        4 => array(
            'stake' => '3',
            'odd' => '1.90',
            'status' => self::VOIDED,
            'result'=>0
        ),

        5 => array(
            'stake' => '5',
            'odd' => '4.21',
            'status' => self::WON,
            'result'=>16.05
        ),
    );
    public function testCalculate()
    {
        foreach(self::$picks as $pick){
            $profitCalculator = new ProfitCalculator($pick['odd'],$pick['stake'],$pick['status']);
            $this->assertEquals($pick['result'],$profitCalculator->calculate());
        }
    }
}
