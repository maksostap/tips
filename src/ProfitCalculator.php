<?php

namespace Tips\src;

use Exception;

class ProfitCalculator
{
    const LOST = 0;
    const WON = 1;
    const HALF_LOST = 2;
    const HALF_WON = 3;
    const VOIDED = 4;
    
    function __construct(private float $odd, private float $stake, private int $status)
    {}

    function calculate()
    {
        switch ($this->status) {
            case self::LOST:
                return $this->stake * -1;
            case self::WON:
                return $this->stake * $this->odd - $this->stake;
            case self::HALF_LOST:
                return $this->stake/-2;
            case self::HALF_WON:
                return $this->stake * $this->odd/2 - $this->stake;
            case self::VOIDED:
                return 0;
            default:
                throw new Exception("Invalid tips status!");
        }
    }
}
